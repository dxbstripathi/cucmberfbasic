package com.testautomation.PageObjects;

//import Base.BaseUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

        WebDriver driver;

	public HomePage(WebDriver driver)
        {
            this.driver=driver;
            PageFactory.initElements(driver, this);
        }

        @FindBy(how=How.ID,using="domain")
        public WebElement SearchTextbox;

        @FindBy(how=How.CLASS_NAME,using="home-btn-compare")
        public WebElement SearchButton;

        public void NavigateToResultPages(String  searchText)
        {
            SearchTextbox.sendKeys(searchText);
            SearchButton.click();
        }








}
