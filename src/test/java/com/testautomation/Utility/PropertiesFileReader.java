package com.testautomation.Utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFileReader {
	
	public Properties getProperty() throws IOException
	{
		FileInputStream inputStream=null;
        Properties properties = new Properties();
        try {        	 
            properties.load(new FileInputStream("src/test/java/com/testautomation/resources/browser-config.properties"));
            properties.load(new FileInputStream("/Users/c_satyendra.mani/Downloads/comtestautomation/src/test/java/com/testautomation/resources/browser-config.properties"));
        } catch (Exception e) {
            System.out.println("Exception: " + e);
       } 
         return properties;	
	}

}
