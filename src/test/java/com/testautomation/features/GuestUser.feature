@FFunctionalTest
Feature: Guest User feature

@SmokeTest
  Scenario: Guest user search for an existing domain
    Given Guest User is on Home Page
    When Guest user search for an existing domain
    Then Counter gets updated and task gets created

  @RegressionTest
  Scenario: Guest user search for an existing domain and validate the response
    Given Guest User is on Home Page
    When Guest user search for an existing domain
    And  Apply filters
    Then The results should change accordingly







