package com.testautomation.Listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestBase {


    //public IOSDriver<MobileElement> driver;

    public static ExtentReports extent;

    public static ExtentTest scenarioDef;

    public static ExtentTest features;

    public static String reportLocation = "/Users/c_satyendra.mani/report/";


    public static final DateFormat timeStamp = new SimpleDateFormat("yyyy_MM_dd");
    public static final DateFormat timeStampHHMM = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    public static java.util.Date date = new Date();



    public  static String folderName = timeStamp.format(date);
    public  static String fileName = timeStampHHMM.format(date);

    public static  String folderPath = reportLocation + folderName;
    public static String filePath = folderPath + "/" + fileName;



    private static TestBase instance;

    public TestBase() {

        super();
    }



}
