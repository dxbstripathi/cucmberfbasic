package com.testautomation.StepDef;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testautomation.Listeners.ExtentReportListener;
import com.testautomation.PageObjects.HomePage;
import com.testautomation.PageObjects.TaskPage;
import com.testautomation.Utility.BrowserUtility;
import com.testautomation.Utility.PropertiesFileReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.Properties;

public class GuestUserSearchDomainStepDef extends ExtentReportListener {

    PropertiesFileReader obj= new PropertiesFileReader();
    private WebDriver driver;

    @Given("^Guest User is on Home Page$")
    public void guest_User_is_on_Home_Page() throws Throwable {

        ExtentTest logInfo = null;
        try {
            test = extent.createTest(Feature.class, "Guest User Task Validation");
            test=test.createNode(Scenario.class, "Guest User Task Validation");
            logInfo=test.createNode(new GherkinKeyword("Given"), "Guest_User_is_on_Home_Page");
            Properties properties=obj.getProperty();
            //driver= BrowserUtility.OpenBrowser(driver, properties.getProperty("browser.name"), properties.getProperty("browser.baseURL"));
            driver=new SafariDriver();
            driver.manage().window().maximize();
            driver.get(properties.getProperty("browser.baseURL"));
            Thread.sleep(3000);
            logInfo.pass("Opened chrome browser and entered url");

            logInfo.addScreenCaptureFromPath(captureScreenShot(driver));

        } catch (AssertionError | Exception e) {
            testStepHandle("FAIL",driver,logInfo,e);
        }

       /* scenarioDef.createNode(new GherkinKeyword("Given"), "guest_User_is_on_Home_Page");
        System.out.println("Navigate Login Page");
        base.Driver.manage().window().maximize();
        base.Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        base.Driver.navigate().to("https://app.hikewise.io/"); */


    }

    @When("^Guest user search for an existing domain$")
    public void guest_user_search_for_an_existing_domain() throws Throwable {

        ExtentTest logInfo=null;
        try {

            logInfo=test.createNode(new GherkinKeyword("When"), "Guest user search for an existing domain$");
            HomePage page = new HomePage(driver);
            page.NavigateToResultPages("gmail.com");
            logInfo.pass("Searching selenium tutorial");
            logInfo.addScreenCaptureFromPath(captureScreenShot(driver));

        } catch (AssertionError | Exception e) {
            testStepHandle("FAIL",driver,logInfo,e);
        }





    }

    @Then("^Counter gets updated and task gets created$")
    public void counter_gets_updated_and_task_gets_created() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @When("^Apply filters$")
    public void Apply_the_filters() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        //sleep(1000);
        TaskPage task = new TaskPage(driver);
        //sleep(1000);
        task.Home();
    }

    @Then("^The results should change accordingly$")
    public void The_results_should_change_accordingly() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        System.out.println("Hello");
    }
}
